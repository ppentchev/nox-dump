#!/usr/bin/python3
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""A nox-dump example that lists the session names and descriptions."""

import pathlib

from nox_dump import collect as nd_collect
from nox_dump import defs as nd_defs
from nox_dump import dump as nd_dump


def main() -> None:
    """The main program: read the Nox sessions file, output the names."""
    ndcfg = nd_defs.Config(
        filename=pathlib.Path("noxfile.py"),
        output_format=nd_defs.OutputFormat.TOML,
        verbose=False,
    )
    ndata = nd_collect.read_noxfile(ndcfg)

    print(f"{len(ndata.sessions)} sessions:")
    max_len = max(len(name) for name in ndata.sessions)
    for sess in ndata.sessions.values():
        print(f"  {sess.name:{max_len}} - {sess.description}")

    toml_output = nd_dump.format_output(ndcfg, ndata)
    print(f"TOML representation: {len(toml_output.splitlines())} lines")


if __name__ == "__main__":
    main()
