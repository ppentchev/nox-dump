# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Nox test definitions for the nox-dump library."""

from __future__ import annotations

import os
import pathlib

import nox


nox.needs_version = ">= 2022.8.7"


def _read_req(flavor: str) -> list[str]:
    """Read a list of Python library dependencies."""
    return [
        line
        for line in pathlib.Path(f"requirements/{flavor}.txt")
        .read_text(encoding="UTF-8")
        .splitlines()
        if line and not line.startswith("#")
    ]


PYFILES = ["examples", "noxfile.py", "src/nox_dump", "unit_tests"]


@nox.session(tags=["check"])
def black(session: nox.Session) -> None:
    """Run the black format checker."""
    session.install(*_read_req("black"))
    session.run("black", "--check", *PYFILES)


@nox.session(name="black-reformat")
def black_reformat(session: nox.Session) -> None:
    """Reformat the Python source code using the black tool."""
    if session.posargs != ["do", "reformat"]:
        raise Exception("The black-reformat session is special")

    session.install(*_read_req("black"))
    session.run("black", *PYFILES)


@nox.session(tags=["check"])
def pep8(session: nox.Session) -> None:
    """Run the flake8 PEP8 compliance checker."""
    session.install(*_read_req("flake8"))
    session.run("flake8", *PYFILES)


@nox.session(tags=["check"])
def mypy(session: nox.Session) -> None:
    """Run the mypy type checker."""
    session.install(*_read_req("mypy"), *_read_req("install"), *_read_req("test"))
    session.run("mypy", *PYFILES)


@nox.session(name="unit-tests", tags=["tests"])
def unit_tests(session: nox.Session) -> None:
    """Run the unit test suite using the pytest tool."""
    session.install(*_read_req("install"), *_read_req("test"))
    session.install(".")
    session.run("pytest", *(session.posargs + ["unit_tests"]))


@nox.session(tags=["tests"])
def examples(session: nox.Session) -> None:
    """Run some of the small programs demonstrating the use of the library."""
    session.install(*_read_req("install"))
    session.install(".")
    cwd = os.getcwd()
    try:
        os.chdir("tests/data/trivial")
        session.run("python3", "../../../examples/list.py")
    finally:
        os.chdir(cwd)
