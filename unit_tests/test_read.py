# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test parsing the Nox configuration."""

from __future__ import annotations

import sys
import typing

from typing import Any

import pytest
import typedload.dataloader

from nox_dump import collect as nd_collect
from nox_dump import defs as nd_defs
from nox_dump import model as nd_model
from nox_dump import dump as nd_dump

from . import utils


def _is_generic_alias(hints: Any, name: str) -> bool:
    """Check whether a described type is the specified generic alias."""
    if getattr(hints, "__name__", "") == name:
        return True
    if getattr(hints, "_name", "") == name:
        return True

    origin = getattr(hints, "__origin__", None)
    return origin is not None and getattr(origin, "_name", "") == name


@pytest.mark.parametrize("load", utils.LOAD_PARAMS, ids=utils.LOAD_IDS)
def test_parse_trivial(load: utils.LoadParams) -> None:
    """Parse the trivial Nox config.

    Please note that due to the way Nox works, this test will populate
    the global nox.registry Nox environments database. Thus, it cannot be
    run together with tests that load Nox environments from different files.
    """
    expected = utils.tomllib.loads(
        (utils.DATA_PATH / "trivial/expected.toml").read_text(encoding="UTF-8")
    )

    # read_noxfile() will produce diagnostic messages on stderr, so put them on
    # a separate line in the pytest output.
    print("", file=sys.stderr)
    cfg = nd_defs.Config(
        filename=utils.DATA_PATH / "trivial/noxfile.py",
        output_format=load.output_format,
        verbose=True,
    )
    data = nd_collect.read_noxfile(cfg)
    text = nd_dump.format_output(cfg, data)

    res_dict = load.loader(text)
    assert isinstance(res_dict, dict)

    nullable_fields = {
        name
        for name, value in typing.get_type_hints(nd_model.Session).items()
        if _is_generic_alias(value, "Optional")
        or (_is_generic_alias(value, "Union") and type(None) in value.__args__)
        or type(value).__name__ == "UnionType"
        and type(None) in value.__args__
    }
    if load.strip_nulls:
        for sess in res_dict["sessions"].values():
            to_remove = []
            for key, value in sess.items():
                if value is not None:
                    continue

                assert key in nullable_fields
                to_remove.append(key)

            for key in to_remove:
                del sess[key]

    assert res_dict == expected

    loader = typedload.dataloader.Loader(pep563=True)
    res = loader.load(res_dict, nd_model.NoxData)
    assert res == data
