# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions and helpers for the unit test suite."""

import json
import pathlib
import sys

from typing import Any, Callable, NamedTuple

from nox_dump import defs as nd_defs

if sys.version_info >= (3, 11):
    import tomllib
else:
    import tomli as tomllib


class LoadParams(NamedTuple):
    """The output format and the function used to parse it."""

    output_format: nd_defs.OutputFormat
    loader: Callable[[str], Any]
    strip_nulls: bool


DATA_PATH = pathlib.Path(__file__).parent.parent / "tests/data"

LOAD_PARAMS = [
    LoadParams(nd_defs.OutputFormat.TOML, tomllib.loads, False),
    LoadParams(nd_defs.OutputFormat.JSON, json.loads, True),
]
LOAD_IDS = ["toml", "json"]

__all__ = ["tomllib"]
