# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Run the nox-dump tool and check its output."""

from __future__ import annotations

import subprocess

import feature_check as fcheck
import pytest

import nox_dump

from nox_dump import defs as nd_defs
from nox_dump import utils as nd_utils

from . import utils


@pytest.mark.parametrize("subdir", ("trivial",))
@pytest.mark.parametrize("load", utils.LOAD_PARAMS, ids=utils.LOAD_IDS)
def test_nox_dump(subdir: str, load: utils.LoadParams) -> None:
    """Run the nox-dump tool for the specified subdirectory's noxfile."""
    expected = utils.tomllib.loads(
        (utils.DATA_PATH / subdir / "expected.toml").read_text(encoding="UTF-8")
    )

    output = subprocess.check_output(
        [
            "nox-dump",
            "-v",
            "-F",
            load.output_format.value,
            "-f",
            utils.DATA_PATH / subdir / "noxfile.py",
        ],
        encoding="UTF-8",
        shell=False,
    )
    raw = load.loader(output)

    if load.output_format != nd_defs.OutputFormat.TOML:
        res = nd_utils.drop_nulls(raw)
    else:
        res = raw

    assert res == expected


def test_features() -> None:
    """Make sure that both nox-dump outputs a parseable feature list."""
    res = fcheck.obtain_features("nox-dump")
    assert res["nox-dump"] == nox_dump.VERSION
