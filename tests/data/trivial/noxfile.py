# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Something about Nox."""

from __future__ import annotations

import nox


PYFILES = [
    "noxfile.py",
    "unit_tests",
]

DEFS = {
    "pytest": ["pytest >= 7, < 8"],
}


def report(session: nox.Session) -> list[str]:
    """Build a command based on the session name."""
    name = session.name if not session.name.startswith("t-unit-tests-") else "t-unit-tests"
    return ["echo", f"This is radio {name} from the pirate satellite"]


@nox.session(name="t-black", tags=["check"])
def black(session: nox.Session) -> None:
    """Run the black format checker.

    This part of the text will not be shown in the description, right?
    """
    session.install("black >= 22, < 23")
    session.run(*report(session), external=True)


@nox.session(name="t-black-reformat")
def black_reformat(session: nox.Session) -> None:
    if session.posargs != ["do", "reformat"]:
        raise Exception("The black-reformat session is special")

    session.install("black >= 22, < 23")
    session.run(*report(session), external=True)


@nox.session(name="t-pep8", tags=["check"])
def pep8(session: nox.Session) -> None:
    """Run the flake8 PEP8 compliance checker."""
    session.install("flake8 >= 5, < 6")
    session.run(*report(session), external=True)


@nox.session(name="t-mypy", tags=["check"])
def mypy(session: nox.Session) -> None:
    """Run the mypy type checker."""
    session.install("mypy >= 0.942", *DEFS["pytest"])
    session.run(*report(session), external=True)


@nox.session(name="t-pylint", python="3.7", tags=["check"])
def pylint(session: nox.Session) -> None:
    """Run the pylint code quality checker."""
    session.install("pylint >= 2.14, < 2.16", *DEFS["pytest"])
    session.run(*report(session), external=True)


@nox.session(name="t-unit-tests", python=["3.8", "3.9", "3.10", "3.11"], tags=["tests"])
def unit_tests(session: nox.Session) -> None:
    """Run the unit test suite using the pytest tool."""
    session.install(*DEFS["pytest"])
    session.run(*report(session), external=True)


@nox.session(name="t-second-fail")
def does_not_work(session: nox.Session) -> None:
    """Fail quickly."""
    session.run("false")


@nox.session(name="t-runner-pep8", tags=["check", "runner"])
def run_pep8(session: nox.Session) -> None:
    """Run the flake8 PEP8 compliance checker, but in yellow."""
    session.install("flake8")
    session.run(*report(session), external=True)


@nox.session(name="t-first-fail")
def does_not_work_either(session: nox.Session) -> None:
    """Fail quickly again."""
    session.run("false")
