# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Parse the Nox configuration and display it."""

from __future__ import annotations

import itertools
import pathlib

from typing import Any

import click

from . import VERSION

from . import collect
from . import defs
from . import dump


FEATURES = {
    "nox-dump": VERSION,
}


def show_features(ctx: click.Context, _param: Any, value: bool) -> None:
    """List the features supported by the program."""
    if not value or ctx.resilient_parsing:
        return
    print(
        "Features: "
        + " ".join(
            f"{name}={version}"
            for name, version in sorted(itertools.chain(defs.FEATURES.items(), FEATURES.items()))
        )
    )
    ctx.exit(0)


@click.command()
@click.option(
    "--features",
    is_flag=True,
    callback=show_features,
    expose_value=False,
    is_eager=True,
    help="list the features supported by the program",
)
@click.option(
    "-F",
    "--output-format",
    type=defs.OutputFormat,
    default=defs.OutputFormat.TOML,
    help="the format in which to display the data",
)
@click.option(
    "-f",
    "--filename",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=pathlib.Path),
    default="noxfile.py",
    help="the path to the noxfile to parse",
)
@click.option("-v", "--verbose", is_flag=True, help="verbose operation; display diagnostic output")
def main(filename: pathlib.Path, output_format: defs.OutputFormat, verbose: bool) -> None:
    """Parse the command-line parameters, read the config, output it."""
    cfg = defs.Config(verbose, filename, output_format)
    data = collect.read_noxfile(cfg)
    print(dump.format_output(cfg, data))
