# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""The data model for the 1.0 nox-dump format."""

import dataclasses

from typing import Dict, List, Optional, Union

import parse_stages


@dataclasses.dataclass(frozen=True)
class FormatVersion:
    """The format version, supposed to be 1.0."""

    major: int
    minor: int


@dataclasses.dataclass(frozen=True)
class Format:
    """The top-level format object to specify the format version."""

    version: FormatVersion


@dataclasses.dataclass(frozen=True)
class Session(parse_stages.TaggedFrozen):
    """A single Nox session as defined in the noxfile."""

    function_name: str
    reuse_venv: bool
    signatures: List[str]
    description: Optional[str] = None
    python: Optional[List[str]] = None
    venv_backend: Optional[str] = None
    venv_params: Optional[Union[str, List[Optional[str]]]] = None

    def get_keyword_haystacks(self) -> List[str]:
        """Look for keywords in the function signatures, too."""
        return [self.name] + self.signatures


@dataclasses.dataclass(frozen=True)
class NoxData:
    """The full representation of the data."""

    format: Format
    sessions: Dict[str, Session]


@dataclasses.dataclass(frozen=True)
class Stage:
    """A single stage, one or more Nox sessions to run."""

    spec: str
    sessions: List[Session]


def current_format() -> Format:
    """Return the current format version."""
    return Format(FormatVersion(1, 0))
