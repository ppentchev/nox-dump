# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Parse a Nox environments file and output the session attributes

The `nox-dump` library attempts, to the best of its ability, to
parse a noxfile.py definitions file for the Nox
testing tool. There is also a `nox-dump` command-line tool that
displays the parsed information in various machine- and human-readable
formats.

# Using the library to parse Nox definitions

To use the `nox-dump` library functions, a `Config` object must be
prepared first. It specifies the path to the noxfile to parse and
the format that the sessions list should be presented in. The `verbose`
flag specifies whether the `nox-dump` library should display diagnostic
messages using the cfg-diag interface.

    from nox_dump import defs as nd_defs
    
    ndcfg = nd_defs.Config(
        filename=pathlib.Path("noxfile.py"),
        output_format=nd_defs.OutputFormat.TOML,
        verbose=False,
    )

The `nox_dump.collect.read_noxfile()` function parses the Nox sessions file:

    from nox_dump import collect as nd_collect
    
    ndata = nd_collect.read_noxfile(ndcfg)
    print(f"{len(ndata.sessions)} sessions:")
    max_len = max(len(name) for name in ndata.sessions)
    for sess in ndata.sessions.values():
        print(f"  {sess.name:{max_len}} - {sess.description}")

The attributes of a `nox-dump` `Session` object correspond to some of a Nox
session's properties, though some liberties have been taken e.g. with
the list of Python interpreters.

The collected data can then be formatted into a multiline string
representation using the `nox_dump.dump.format_output()` function:

    from nox_dump import dump as nd_dump
    
    toml_output = nd_dump.format_output(ndcfg, ndata)
    print(f"TOML representation: {len(toml_output.splitlines())} lines")

These code snippets are taken directly from the `examples/list.py` file.

# Using the nox-dump command-line utility

The `nox-dump` utility must be pointed to the noxfile to parse using
the required `-f` (`--filename`) parameter. The output format, TOML by
default, may be specified using the `-F` (`--output-format`) option:

    nox-dump -f tests/data/trivial/noxfile.py -F json

For the present, `nox-dump` does not try to prettify its JSON output;
an external tool such as jq or Python's json.tool may be
used to do that.
"""  # noqa: W293


VERSION = "1.0.0"
