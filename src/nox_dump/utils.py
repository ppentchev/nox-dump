# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Helper functions for the `nox-dump` tool."""

import functools

from typing import Any


@functools.singledispatch
def drop_nulls(value: Any) -> Any:
    """Drop any null list or dictionary values."""
    return value


@drop_nulls.register
def _drop_nulls_list(value: list) -> Any:  # type: ignore[type-arg]
    """Drop any null list values."""
    return [drop_nulls(item) for item in value if item is not None]


@drop_nulls.register
def _drop_nulls_dict(value: dict) -> Any:  # type: ignore[type-arg]
    """Drop any null dict values."""
    return {i_key: drop_nulls(i_value) for i_key, i_value in value.items() if i_value is not None}
