# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the nox-dump library."""

import dataclasses
import enum
import pathlib

import cfg_diag


FEATURES = {
    "dump": "1.0",
    "dump-json": "1.0",
    "dump-toml": "1.0",
    "expr": "1.0",
}


class OutputFormat(str, enum.Enum):
    """The selected output format for the data representation."""

    JSON = "json"
    TOML = "toml"


@dataclasses.dataclass(frozen=True)
class Config(cfg_diag.Config):
    """Runtime configuration for the nox-dump tool."""

    filename: pathlib.Path
    output_format: OutputFormat
