# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Format the data for output as per the configuration."""

from __future__ import annotations

import dataclasses
import json

import tomli_w

from . import defs
from . import model
from . import utils


def _format_unknown(fmt: defs.OutputFormat, data: model.NoxData) -> str:
    """Actually format the data dictionary."""
    raise NotImplementedError(f"Don't know how to format {fmt}: {data!r}")


def _format_json(_fmt: defs.OutputFormat, data: model.NoxData) -> str:
    """Format the output as a TOML object."""
    return json.dumps(dataclasses.asdict(data))


def _format_toml(_fmt: defs.OutputFormat, data: model.NoxData) -> str:
    """Format the output as a TOML object."""
    return tomli_w.dumps(utils.drop_nulls(dataclasses.asdict(data)))


_FORMATTERS = {
    defs.OutputFormat.JSON: _format_json,
    defs.OutputFormat.TOML: _format_toml,
}


def format_output(cfg: defs.Config, data: model.NoxData) -> str:
    """Format the parsed configuration as requested."""
    return _FORMATTERS.get(cfg.output_format, _format_unknown)(cfg.output_format, data)
