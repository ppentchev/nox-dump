# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Load the Nox configuration from the noxfile."""

from __future__ import annotations

import functools
import importlib.machinery as imp_mach
import importlib.util as imp_util

from typing import Any, List, NoReturn, Optional

import nox.registry

from . import defs
from . import model


def _str_or_none(value: Any) -> str | None:
    """Return a string if the value is a string, None if it is anything else."""
    if isinstance(value, str):
        return value

    return None


def _cvt_python_error(python: Any, sess_name: str) -> NoReturn:
    """Error out on invalid values for the "python" session parameter."""
    raise NotImplementedError(
        f"Don't know what to do with the {python!r} value for the {sess_name} session"
    )


@functools.singledispatch
def _cvt_python(python: Any, sess_name: str) -> Optional[List[str]]:
    """Parse the various values for the "python" session parameter."""
    _cvt_python_error(python, sess_name)


@_cvt_python.register
def _cvt_python_none(_python: None, _sess_name: str) -> Optional[List[str]]:
    """Pass a None value on."""
    return None


@_cvt_python.register
def _cvt_python_bool(python: bool, sess_name: str) -> Optional[List[str]]:
    """Convert False to None, error out on True."""
    if python:
        _cvt_python_error(python, sess_name)

    return None


@_cvt_python.register
def _cvt_python_str(python: str, _sess_name: str) -> Optional[List[str]]:
    """Wrap a string value as a single-element list."""
    return [python]


@_cvt_python.register
def _cvt_python_list(python: list, sess_name: str) -> Optional[List[str]]:  # type: ignore[type-arg]
    """Make sure a list only contains strings."""
    res: list[str] = [item for item in python if isinstance(item, str)]
    if res != python:
        _cvt_python_error(python, sess_name)

    return python


def _cvt_description(session: Any) -> str | None:
    """Get the first line of the session's docstring if any."""
    doc = getattr(session, "__doc__", None)
    if doc is None:
        return None
    if not isinstance(doc, str):
        raise NotImplementedError(
            f"Don't know how to handle a {type(doc).__name__} description: {doc!r}"
        )

    return doc.splitlines()[0]


def read_noxfile(cfg: defs.Config) -> model.NoxData:
    """Read the configuration."""
    cfg.diag(lambda: f"Loading a noxfile: {cfg.filename}")
    loader = imp_mach.SourceFileLoader("noxfile", str(cfg.filename))
    spec = imp_util.spec_from_loader(loader.name, loader)
    assert spec is not None
    mod = imp_util.module_from_spec(spec)
    loader.exec_module(mod)

    sessions = nox.registry.get()
    cfg.diag(lambda: f"Loaded {len(sessions)} sessions: {list(sessions)}")
    return model.NoxData(
        format=model.current_format(),
        sessions={
            name: model.Session(
                description=_cvt_description(session),
                function_name=session.func.__name__,
                name=name,
                python=_cvt_python(session.python, name),
                reuse_venv=bool(session.reuse_venv),
                signatures=[session.func.__name__],
                venv_backend=_str_or_none(session.venv_backend),
                venv_params=[_str_or_none(item) for item in session.venv_params]
                if isinstance(session.venv_params, list)
                else _str_or_none(session.venv_params),
                tags=session.tags,
            )
            for name, session in sessions.items()
        },
    )
